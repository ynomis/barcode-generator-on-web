# Barcode Generator

Description
-----------
A quick weekend project to provide a barcode printout in web browser based off Avery template. Challenges are:

* Serial numbers should be generated sequentially and consistently;
* Prevent same number being created from different user session;
* Host the project with others within uwsgi services;

Usage
-----
Visit the portal and pick the type and quantity of labels, then click the button next to it. When the label page loads up, click the `Print` function on the browser.

It is tuned for Chrome, and from the settings, please set margin to 'None' and disable 'Header/Footer'

Dependencies
------------
Code is developed by Python 3 + Django 1.8, extra packages are PyBarcode 0.7, and Pillow (a PIL port for Python 3, optional).
