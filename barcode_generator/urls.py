from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'barcode_generator.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', include('barcoder.urls')),
    url(r'^barcoder/', include('barcoder.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
