from django.db import models
from datetime import datetime

class Tracker(models.Model):
    """
    Table to track all serial numbers used within Proove internal system.
    such as, sample id, patient id, etc.
    """

    type = models.CharField(max_length = 2, unique = True)
    desc = models.CharField(max_length = 256)
    serial = models.IntegerField()
    copies = models.IntegerField()
    updated_at = models.DateTimeField(default = datetime.now)
    status = models.BooleanField(default = True)
    sort_order = models.IntegerField(default = 0)
    
    @classmethod
    def next(cls, labelType):
        ser = cls.objects.get(type = labelType)
        ser.serial += 1
        ser.update_at = datetime.now
        ser.save()
        return ser

    def __str__(self):
        return "{}{}".format(self.type, str(self.serial))

    class Meta:
        ordering = ['sort_order', 'desc']
