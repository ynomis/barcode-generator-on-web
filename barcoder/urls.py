from django.conf.urls import patterns, url
from barcoder import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<code>[a-zA-Z0-9-_]+)$', views.generate_barcode, name='generate'),
    url(r'^svg/(?P<code>[a-zA-Z0-9-_]+)$', views.generate_barcode_svg, name='generate_svg'),
    url(r'^avery_5167/(?P<labelType>[a-zA-Z0-9]{1,2})$', views.avery_5167, name='avery_5167'),
    url(r'^avery_5167/(?P<labelType>[a-zA-Z0-9]{1,2})/(?P<qty>\d+)$', views.avery_5167, name='avery_5167_custom'),
)
