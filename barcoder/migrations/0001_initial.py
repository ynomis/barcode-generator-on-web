# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tracker',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('type', models.CharField(max_length=2)),
                ('desc', models.CharField(max_length=256)),
                ('serial', models.IntegerField()),
                ('copies', models.IntegerField()),
                ('updated_at', models.DateTimeField(default=datetime.datetime.now)),
            ],
        ),
    ]
