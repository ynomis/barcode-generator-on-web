# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('barcoder', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tracker',
            options={'ordering': ['sort_order', 'desc']},
        ),
        migrations.AddField(
            model_name='tracker',
            name='sort_order',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='tracker',
            name='status',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='tracker',
            name='type',
            field=models.CharField(max_length=2, unique=True),
        ),
    ]
